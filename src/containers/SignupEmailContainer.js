import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  NetInfo,
  BackHandler,
  Alert
} from 'react-native';
import FloatingLabelInput from '../components/FloatingLabelInput';
import LinearGradient from 'react-native-linear-gradient';
import SwitchSelector from "react-native-switch-selector";

var { height, width } = Dimensions.get('window');


const options = [
    { label: "Employer", value: "0" },
    { label: "Employee", value: "1" }
  ];
  
export default class SignupEmail extends Component{
    constructor(){
        super(); 
        this.state = {
            firstName: '',
            lastName: '',
            companyName: '',
            email: '',
            password: '',
            confirmPassword: '',
            license: '', 
            passSecure: false,
            validate: false,
            passwordError: false,
            selectedUserType: 1
        }
    }


    SignUp(email, password){
        try {
            firebase
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then(user => { 
                       console.log(user);
                });
        } 
        catch (error) {
            console.log(error.toString(error));
          }
    }

    getFirstName(text){
        this.setState({ firstName: text })
    }

    getLastName(text){
        this.setState({ lastName: text })
    }

    getCompanyName(text){
        this.setState({ companyName: text })
    }

    getEmail(text){
        this.setState({ email: text, validate: false })
    }

    getPassword(text){
        this.setState({ password: text })
    }

    getConfirmPassword(text){
        this.setState({ confirmPassword: text })
        if(this.state.password !== this.state.confirmPassword)
            this.setState({ passwordError : true })
        else
            this.setState({ passwordError: false })
    }

    viewPassword() {
        this.setState({
            passSecure: !this.state.passSecure
        })
    }

    getLicense(text){
        this.setState({ license: text })
    }


    submit(){
        if(this.state.email == '')
            this.setState({ validate: true })
        else{
            this.props.navigation.navigate('Home');
        }
    }


    render(){
        return(
            <ScrollView style={{ backgroundColor: '#FFFFFF', flex:1}}>
                <StatusBar backgroundColor={'#000'}/>
                <TouchableOpacity style={{ width: width/15, left: width/1.15, marginTop: height/40}} onPress={() => this.props.navigation.navigate('Welcome')}>
                    <Image source={require('../assets/close.png')} style={{height: height/25, width: height/20, alignSelf: 'center', resizeMode: 'contain'}}/>
                </TouchableOpacity>
                <View style={{ alignItems: 'center'}}>
                    <FloatingLabelInput
                        label= "First Name"
                        value={this.state.firstName}
                        width={width / 1.2}
                        onChangeText={(text) => this.getFirstName(text)}
                    />
                    
                    <FloatingLabelInput
                        label= "Last Name"
                        value={this.state.lastName}
                        width={width / 1.2}
                        onChangeText={(text) => this.getLastName(text)}
                    />
                    
                    <FloatingLabelInput
                        label= "Company Name"
                        value={this.state.companyName}
                        width={width / 1.2}
                        onChangeText={(text) => this.getCompanyName(text)}
                    />
                    
                    <FloatingLabelInput
                        label= "Email"
                        value={this.state.email}
                        width={width / 1.2}
                        onChangeText={(text) => this.getEmail(text)}
                    />
                    {
                        (this.state.validate)
                        ? <Text style={{ color: 'red', right: width/3, bottom: height/40}}>Enter Email</Text>
                        : null
                    }

                    <View style={{ flexDirection: "row", alignSelf: 'center', paddingLeft: width/12}}>
                        <FloatingLabelInput
                            label="Password"
                            ref={ref => this.customInput2 = ref}
                            refInner="innerTextInput2"
                            value={this.state.password}
                            width={width / 1.2}
                            secureTextEntry={this.state.passSecure}
                            onChangeText={(text) => this.getPassword(text)}
                            style={styles.passwordStyle} />

                        <TouchableOpacity onPress={() => this.viewPassword()} style={{  flex:1, right: width/10}}>
                            <Image
                                source={(this.state.passSecure) ? require('../assets/eye-open.png') : require('../assets/eye-closed.png')}
                                resizeMode={"contain"}
                                style={{ height: height / 13, width: width / 13, left: height / 500, top: height / 120 }}
                            />
                        </TouchableOpacity>
                    </View>


                    <View style={{ flexDirection: "row", alignSelf: 'center', paddingLeft: width/12}}>
                        <FloatingLabelInput
                            label="Confirm Password"
                            ref={ref => this.customInput2 = ref}
                            refInner="innerTextInput2"
                            value={this.state.confirmPassword}
                            width={width / 1.2}
                            secureTextEntry={this.state.passSecure}
                            onChangeText={(text) => this.getConfirmPassword(text)}
                            style={styles.passwordStyle} />

                        <TouchableOpacity onPress={() => this.viewPassword()} style={{  flex:1, right: width/10}}>
                            <Image
                                source={(this.state.passSecure) ? require('../assets/eye-open.png') : require('../assets/eye-closed.png')}
                                resizeMode={"contain"}
                                style={{ height: height / 13, width: width / 13, left: height / 500, top: height / 120 }}
                            />
                        </TouchableOpacity>
                    </View>
                    {
                        (!this.state.passwordError)
                            ? <Text style={{ color: 'red', right: width/4}}>Password Mismatch</Text>
                            : null
                    }

                    <FloatingLabelInput
                        label= "License No."
                        value={this.state.license}
                        width={width / 1.2}
                        onChangeText={() => this.getLicense()}
                    />
                    <TouchableOpacity style={{ width: width/1.1, borderRadius: 50, justifyContent: 'center', alignSelf: 'center', marginTop: height/30}} onPress={() => this.submit()}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#43bfe6', '#0875a3', '#07435c']} style={{ justifyContent:'center', height: height/10, borderRadius: height/75}}>
                            <Text style={{ alignSelf: 'center', color: '#FFFFFF', fontWeight: '600', fontSize: height/35}}>Continue</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    signIn: {
        color: "#D72163",
        alignSelf: "center",
        fontSize: 14,
        marginTop: height / 40,
        marginBottom: 60,
    },
    usernameStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
    },
    passwordStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
        borderWidth: 1
    }

});