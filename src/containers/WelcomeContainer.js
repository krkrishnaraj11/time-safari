import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  NetInfo,
  BackHandler
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
var { height, width } = Dimensions.get('window');
import { NativeModules } from 'react-native';
const UsageStats = NativeModules.UsageStats;

export default class Welcome extends Component{
    constructor(){
        super();
        this.state = {

        }
    }

    render(){
        return(
            <View style={{ backgroundColor: '#FFFFFF', flex:1}}>
                <StatusBar backgroundColor={'#FFFFFF'}/>
                <Text style={{ color: '#000', fontSize: height/22, fontWeight: '400', alignSelf: 'center', marginTop: height/6}}>Time Safari</Text>
                <Image source={require('../assets/welcome.jpg')} style={{height: height/3, width: width,alignSelf: 'center', resizeMode: 'contain'}}/>
                <View>
                    <TouchableOpacity style={{ width: width/1.1, borderRadius: 50, justifyContent: 'center', alignSelf: 'center', marginTop: height/10}} onPress={() => this.props.navigation.navigate('LogIn')}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#43bfe6', '#0875a3', '#07435c']} style={{ justifyContent:'center', height: height/10, borderRadius: height/75}}>
                            <Text style={{ alignSelf: 'center', color: '#FFFFFF', fontWeight: '600', fontSize: height/35}}>Login</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: width/1.1, borderRadius: height/75, justifyContent: 'center', alignSelf: 'center', marginTop: height/20, backgroundColor: '#e8eced', height: height/10}} onPress={() => this.props.navigation.navigate('SignupEmail')}>
                            <Text style={{ alignSelf: 'center', color: '#000000', fontWeight: '600', fontSize: height/35}}>Signup</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}