import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  NetInfo,
  BackHandler
} from 'react-native';
import {API} from '../config/API';
import FloatingLabelInput from '../components/FloatingLabelInput';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
var { height, width } = Dimensions.get('window');


export default class Login extends Component{
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            passSecure: false,
            loginResponse: {},
            myKey: '',
            registered: false
        }
    }

    getEmail(text){
        this.setState({ email: text })
    }

    getPassword(text){
        this.setState({ password: text })
    }

    viewPassword() {
        this.setState({
            passSecure: !this.state.passSecure
        })
    }


    async storeItem(key, item) {
        try {
            var jsonOfItem = await AsyncStorage.setItem(key, item);
            return jsonOfItem;
        } catch (error) {
            console.log(error.message);
        }
    }

    async getItem(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            this.setState({ myKey: JSON.parse(value) });
            console.log("$$", this.state.myKey)
            if (this.state.myKey.response_code == "success") {
                this.setState({
                    registered: true
                })
            }
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
        return Promise.resolve(this.state.registered)
    }


    login = () => {
        var data = {
            username: this.state.email,
            password: this.state.password
        }
        console.log(data)

        axios({
            url: API.login,
            method: 'post',
            data: data,
            headers: {
                'Accept': 'application/json',
            }
        })
            .then((res) => {
                console.log("res",res.data);
                this.setState({
                    loginResponse: res.data
                })
                var login_token = res.data.token;
                this.storeItem("loginResponse", JSON.stringify(res.data));
                console.log("AWAIT:", this.getItem('loginResponse'))

                this.props.navigation.navigate('Home');

            })
            .catch((error) => {
                console.log(error);
            });
    }



    render(){
        return(
            <View style={{ backgroundColor: '#FFFFFF', flex:1}}>
                <StatusBar backgroundColor={'#FFFFFF'}/>
                <TouchableOpacity style={{ width: width/15, marginLeft: height/40}} onPress={() => this.props.navigation.navigate('Welcome')}>
                <Image source={require('../assets/back.png')} style={{height: height/20, width: height/20, alignSelf: 'center', resizeMode: 'contain'}}/>
                </TouchableOpacity>
                <Text style={{ marginLeft: width/10, marginTop: width/10, fontSize: height/30,}}>Login</Text>
                <View style={{ marginTop: 20, alignItems: 'center'}}>
                    <FloatingLabelInput
                        label= "Email"
                        value={this.state.email}
                        width={width / 1.2}
                        onChangeText={(text) => this.getEmail(text)}
                    />
                </View>
                    <View style={{ flexDirection: "row", alignSelf: 'center', paddingLeft: width/12}}>
                        <FloatingLabelInput
                            label="Password"
                            ref={ref => this.customInput2 = ref}
                            refInner="innerTextInput2"
                            value={this.state.password}
                            width={width / 1.2}
                            secureTextEntry={this.state.passSecure}
                            onChangeText={(text) => this.getPassword(text)}
                            style={styles.passwordStyle} />

                        <TouchableOpacity onPress={() => this.viewPassword()} style={{  flex:1, right: width/10}}>
                            <Image
                                source={(this.state.passSecure) ? require('../assets/eye-open.png') : require('../assets/eye-closed.png')}
                                resizeMode={"contain"}
                                style={{ height: height / 13, width: width / 13, left: height / 500, top: height / 120 }}
                            />
                        </TouchableOpacity>
                    </View>
                <View>
                    <TouchableOpacity style={{ width: width/1.1, borderRadius: 50, justifyContent: 'center', alignSelf: 'center', marginTop: height/10}} onPress={() => this.login()}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#43bfe6', '#0875a3', '#07435c']} style={{ justifyContent:'center', height: height/10, borderRadius: height/75}}>
                            <Text style={{ alignSelf: 'center', color: '#FFFFFF', fontWeight: '600', fontSize: height/35}}>Login</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    signIn: {
        color: "#D72163",
        alignSelf: "center",
        fontSize: 14,
        marginTop: height / 40,
        marginBottom: 60,
    },
    usernameStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
    },
    passwordStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
        borderWidth: 1
    }

});