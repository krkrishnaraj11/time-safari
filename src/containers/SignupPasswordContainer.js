import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  NetInfo,
  BackHandler
} from 'react-native';
import FloatingLabelInput from '../components/FloatingLabelInput';
import LinearGradient from 'react-native-linear-gradient';
var { height, width } = Dimensions.get('window');

export default class SignupPassword extends Component{
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            passSecure: false
        }
    }


    getPassword(text){
        this.setState({ password: text })
    }

    submit(){
        if(this.state.email == '')
            this.setState({ validate: true })
        else{
            this.props.navigation.navigate('Home');
        }
    }


    render(){
        return(
            <View style={{ backgroundColor: '#FFFFFF', flex:1}}>
                <StatusBar backgroundColor={'#FFFFFF'}/>
                <TouchableOpacity style={{ width: width/15, left: width/1.15}} >
                <Image source={require('../assets/close.png')} style={{height: height/20, width: height/20, alignSelf: 'center', resizeMode: 'contain'}}/>
                </TouchableOpacity>
                <Text style={{ marginLeft: width/10, marginTop: width/10, fontSize: height/20,}}>Choose your Password</Text>
                <View style={{ marginTop: 20, alignItems: 'center', height: height/3}}>
                    <FloatingLabelInput
                        label= "At least 8 characters"
                        value={this.state.password}                        
                        secureTextEntry={true}
                        width={width / 1.2}
                        onChangeText={() => this.getPassword()}
                    />
                </View>
                <View style={{ backgroundColor: '#e8eced' }}>
                    <TouchableOpacity style={{ width: width/1.1, borderRadius: 50, justifyContent: 'center', alignSelf: 'center', marginTop: height/10}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#43bfe6', '#0875a3', '#07435c']} style={{ justifyContent:'center', height: height/10, borderRadius: height/75 }}>
                            <Text style={{ alignSelf: 'center', color: '#FFFFFF', fontWeight: '600', fontSize: height/35}}>Continue</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    signIn: {
        color: "#D72163",
        alignSelf: "center",
        fontSize: 14,
        marginTop: height / 40,
        marginBottom: 60,
    },
    usernameStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
    },
    passwordStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
        borderWidth: 1
    }

});