import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
  NetInfo,
  BackHandler
} from 'react-native';
import FloatingLabelInput from '../components/FloatingLabelInput';
import LinearGradient from 'react-native-linear-gradient';
var { height, width } = Dimensions.get('window');


const DATA = [
    {
      title: 'WASHINGTON',
    },
    {
      title: 'MONTANA',
    },
    {
      title: 'IDAHO',
    },
  ];

  

export default class Home extends Component{
    constructor(){
        super();
        this.state = {
            name: 'Gabie Sheber',
            email: 'example@gmail.com'
        }
    }

    getEmail(text){
        this.setState({ email: text })
    }

    getPassword(text){
        this.setState({ password: text })
    }

    viewPassword() {
        this.setState({
            passSecure: !this.state.passSecure
        })
    }

    renderData(item){
        return(
            <View>
                <TouchableOpacity style={styles.countryTile} onPress={() => this.props.navigation.navigate('Employees')}>
                    <View>
                        <Text style={styles.countryName}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }


    render(){
        return(
            <View style={{ backgroundColor: '#f4f4f4', flex:1}}>
                <StatusBar backgroundColor={'#f4f4f4'}/>
                <TouchableOpacity style={{ width: width/15, marginLeft: height/40, marginTop: height/30}} onPress={() => this.props.navigation.toggleDrawer()}>
                <Image source={require('../assets/menu.png')} style={{height: height/20, width: height/20, alignSelf: 'center', resizeMode: 'contain'}}/>
                </TouchableOpacity>

                <View style={{ borderWidth:1, padding: height/50, marginTop: height/25, flexDirection: 'row', borderBottomColor: '#00c2f2', borderTopColor: '#f4f4f4', borderLeftColor: '#f4f4f4', borderRightColor: '#f4f4f4', margin:height/50}}>
                    <Image source={require('../assets/profile_pic.jpeg')} style={styles.profPicStyle}/>
                    <View style={{ flexDirection: 'column', marginLeft: height/25 }}>
                        <Text style={styles.name}>{this.state.name}</Text>
                        <Text style={styles.email}>{this.state.email}</Text>
                    </View>
                    <TouchableOpacity style={{ alignSelf: 'center', left: width/10}}>
                        <Image source={require('../assets/sub-menu.png')} style={{height: height/25, width: height/25, alignSelf: 'center', resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={DATA}
                    renderItem={({ item }) =>  this.renderData(item)}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    profPicStyle: {
        height: height/10, 
        width: height/10, 
        borderRadius: height/3,
    },
    name: {
        fontSize: height/30,
        fontWeight: '400'
    },
    countryTile: {
        borderLeftWidth: width/45,
        alignSelf: 'center',
        width: width/1.3,
        height: height/5,
        borderLeftColor: '#5b86e5',
        backgroundColor: '#FFF',
        marginBottom: height/15,
        borderTopLeftRadius: height/75,
        borderBottomLeftRadius: height/75,
        justifyContent: 'center',
         alignItems: 'center'
    },
    countryName: {

    }
});