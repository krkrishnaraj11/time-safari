import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  View,
  Dimensions,
  BackHandler
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";

var { height, width } = Dimensions.get('window');

export default class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props); 
    this.state={
      connection: true,
      status: true
    }
    this._bootstrapAsync();
    // this.props.navigation.navigate('Welcome')
  }

//   componentDidMount() {
//     // BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressed);
//     const unsubscribe = NetInfo.addEventListener(state => {
//       console.log(state.isConnected);
//       this.handleConnectionChange(state.isConnected);
//     })

//     NetInfo.fetch().then(state => {
//       this.setState({ status: state.isConnected })
//       console.log("status", this.state.status)
//     })
// //     NetInfo.fetch().then((connectInfo) => {
// //       console.log(connectInfo.type);
// //       this.setState({
// //         connection: (connectInfo.type == 'none') ? false : true
// //       })
// //     })
//   }

//   componentWillUnmount() {
//     // BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressed);
//     NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
//     // unsubscribe();
//   }

// handleConnectionChange = (isConnected) => {
//   this.setState({ status: isConnected });
//   console.log(`is connected: ${this.state.status}`);
// }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('loginResponse')
    const token = (userToken == null) ? null : JSON.parse(userToken).token;

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(token ? 'Home' : 'Welcome');
  };

  // Render any loading content that you like here
  render() {``
    return (
      <View style={{justifyContent: 'center', alignItems: 'center', height: height, width: width}}>
        <ActivityIndicator size="large"/>
        <StatusBar barStyle="default" />
      </View>
    );
  }
}
