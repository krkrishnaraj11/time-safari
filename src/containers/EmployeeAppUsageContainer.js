import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions,
  NetInfo,
  BackHandler,
  Platform
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
var { height, width } = Dimensions.get('window');

export default class EmployeeAppUsage extends Component{
    constructor(){
        super();
        this.state = {
        }
    }


    render(){
        return(
            <View style={{ backgroundColor: '#FFFFFF', flex:1}}>
                <StatusBar backgroundColor={'black'}/>
                <View style={{ flexDirection: 'row'}}>
                    <TouchableOpacity style={{ width: width/15, marginLeft: height/40}} onPress={() =>this.props.navigation.goBack()}>
                    <Image source={require('../assets/back.png')} style={styles.backStyle}/>
                    </TouchableOpacity>
                    <Image source={require('../assets/profile_pic.jpeg')} style={styles.profPicStyle}/>
                </View>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#43bfe6', '#0875a3', '#07435c']} style={{ justifyContent:'center', height: height/400, width: width, marginTop: height/50}}/>

                <Text style={{ color: "#618ce6", fontSize: height/30, fontWeight: "bold", padding: height/25}}>Application Usage Time</Text>

                <View style={{ borderWidth:1, width: width/1.3, alignSelf: 'center', borderColor: '#EFEFEF', flexDirection: 'row', justifyContent: 'space-around', borderRadius: height/55}}>
                    <View style={{ width: width/1.8}}>
                        <Text style={{ color: '#2D4D66', fontWeight:'bold', fontSize: height/35, marginLeft: width/20, marginTop: height/40 }}>Amazon</Text>
                        <Text style={{ color: '#5F8AE5', fontSize: height/35, fontWeight: 'bold', textAlign: 'right', marginRight: width/43}}>12m 23s</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: width/20, width: width/2.6, marginTop: height/100}}>
                            <Text style={{ fontWeight:'200', fontSize: height/44 }}>2019.11.14</Text>
                            <Text style={{ fontWeight:'200', fontSize: height/44 }}>15:30:55</Text>
                        </View>
                    </View>
                    <View style={{ width: width/5, backgroundColor: 'black', height: height/5, borderBottomLeftRadius: height/25,borderTopRightRadius: height/55, borderBottomRightRadius: height/55}}>
                        <Image source={{uri: this.state.profilePic}} style={{ height: height/8, width: height/8, borderRadius: height/15, alignSelf: 'center'}} />
                    </View>
                <View>

                    </View>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    signIn: {
        color: "#D72163",
        alignSelf: "center",
        fontSize: 14,
        marginTop: height / 40,
        marginBottom: 60,
    },
    usernameStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
    },
    passwordStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
        borderWidth: 1
    },
    backStyle:{
        height: height/20, 
        width: height/20, 
        
        marginTop: height/20,
        alignSelf: 'center', 
        resizeMode: 'contain'
    },
    profPicStyle: {
        height: height/7, 
        width: height/7, 
        borderRadius: height/3,
        left: width/4,
        marginTop: height/100
    },
});