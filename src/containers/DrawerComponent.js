import React, { Component } from 'react';
import {NavigationActions} from 'react-navigation';

import {Text, View, StyleSheet, AsyncStorage, Dimensions, Image, TouchableOpacity} from 'react-native';
var {height, width} = Dimensions.get('screen');

export default class CustomDrawerContentComponent extends Component {

    constructor(){
        super();
        this.state = {
            profilePic: '',
            name: 'John Kayle',
            job: 'Employee',
            uid: ''
        }
    }

    async getUserDetails(){
        let value = await AsyncStorage.getItem('loginResponse');
        let data = JSON.parse(value)
        console.log(data)
        this.setState({
            profilePic: data.profile_pic,
            name: data.user.first_name + " " + data.user.last_name,
            uid: data.uid
        })
    }

    componentWillMount(){
        // this.getUserDetails();
    }

  navigateToScreen = ( route ) =>(
    () => {
      const navigateAction = NavigationActions.navigate({
        routeName: route
      });
      this.props.navigation.dispatch(navigateAction);
    })

    logout(){
        NavigationActions.navigate({
            routeName: 'Welcome'
        })
    }

   render(){
     return(
      <View style={styles.container}>
      <View style={styles.headerContainer}>
          <View style={{flex: 1, width: width/1.4, justifyContent: 'center', backgroundColor: '#283943'}} >
              <Image source={require('../assets/profile_pic.jpeg')} style={{ height: height/8, width: height/8, borderRadius: height/15, alignSelf: 'center',borderWidth: width/125, borderColor: '#FFF'}} />
              <Text style={styles.name}>{this.state.name}</Text>
              <Text style={styles.nametag}>{this.state.job}</Text>
              <Text style={styles.uid}>{this.state.uid}</Text>
          </View>
      </View>
      <View style={styles.screenContainer}>
          <TouchableOpacity style={[styles.screenStyle, (this.props.activeItemKey=='Home') ? styles.activeBackgroundColor : null]}>
              <Text style={[styles.screenTextStyle, (this.props.activeItemKey=='Home') ? styles.selectedTextStyle : null]} onPress={this.navigateToScreen('Home')}>HOME</Text>
          </TouchableOpacity>

          <View style={{ backgroundColor: '#446073', width: width/2, height: width/250, alignSelf: 'center'}}/>

          <TouchableOpacity style={[styles.screenStyle, (this.props.activeItemKey=='Employees') ? styles.activeBackgroundColor : null]}>
              <Text style={[styles.screenTextStyle, (this.props.activeItemKey=='Employees') ? styles.selectedTextStyle : null]} onPress={this.navigateToScreen('Employees')}>EMPLOYEES</Text>
          </TouchableOpacity>

          <View style={{ backgroundColor: '#446073', width: width/2, height: width/250, alignSelf: 'center'}}/>

          <TouchableOpacity style={[styles.screenStyle, (this.props.activeItemKey=='Logout') ? styles.activeBackgroundColor : null]}>
              <Text style={[styles.screenTextStyle, (this.props.activeItemKey=='Logout') ? styles.selectedTextStyle : null]} onPress={this.logout()}>Logout</Text>
          </TouchableOpacity>

          <View style={{ backgroundColor: '#446073', width: width/2, height: width/250, alignSelf: 'center'}}/>
      </View>
  </View>
     )
   }
}

const styles = StyleSheet.create({
  container: {
      alignItems: 'center'
  },
  headerContainer: {
      height: height/3,
      justifyContent: 'center',
  },
  name: {
      color: '#fff8f8',
      alignSelf: 'center',
      marginTop: height/45,
      fontSize: height/40,
      fontWeight: '500'
  },
  nametag: {
    color: '#446073',
    alignSelf: 'center',
    fontSize: height/55,
    fontWeight: '500'
},
  uid: {
    color: '#fff8f8',
    alignSelf: 'center',
    fontSize: height/40,
    fontWeight: '300'
  },
  screenContainer: { 
      backgroundColor: '#283943',
      height: height
  },
  screenStyle: {
      height: height/12,
    //   marginTop: height/50,
      justifyContent: 'center',
      flexDirection: 'column',
      alignItems: 'center',
      width: width/1.4
  },
  screenTextStyle:{
      fontSize: 20,
      textAlign: 'center',
      textAlign: 'center'
  },
  selectedTextStyle: {
      fontWeight: 'bold',
      color: '#00adff'
  },
  activeBackgroundColor: {
      backgroundColor: '#283943'
  }
});