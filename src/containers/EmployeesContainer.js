import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions,
  NetInfo,
  BackHandler
} from 'react-native';
import FloatingLabelInput from '../components/FloatingLabelInput';
import LinearGradient from 'react-native-linear-gradient';
var { height, width } = Dimensions.get('window');

const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'First Item',
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'Second Item',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Third Item',
    },
  ];

export default class Employees extends Component{
    constructor(){
        super();
        this.state = {
            
        }
    }

    renderEmployee(item){
        return(
            <TouchableOpacity style={{ height: height/10, flexDirection: 'row', paddingLeft: width/30 }} onPress={() => this.props.navigation.navigate('EmployeeInfo')}>
                 <Image source={require('../assets/profile_pic.jpeg')} style={styles.profPicStyle}/>
                <Text style={{ height: height/10, marginLeft: width/25, top: height/50}}>{item.title}</Text>
            </TouchableOpacity>
        )
    }


    render(){
        return(
            <View style={{ backgroundColor: '#FFFFFF', flex:1}}>
                <StatusBar backgroundColor={'#FFFFFF'}/>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around'}}>
                    <TouchableOpacity style={{ width: width/15}} onPress={() => this.props.navigation.toggleDrawer()}>
                        <Image source={require('../assets/menu.png')} style={{height: height/20, width: height/20, alignSelf: 'center', resizeMode: 'contain'}}/>
                    </TouchableOpacity> 
                    <Text style={{ fontSize: height/30}}>List of Employees</Text>
                    <TouchableOpacity style={{ width: width/15, marginLeft: height/40}}>
                        <Image source={require('../assets/share.png')} style={{height: height/20, width: height/20, alignSelf: 'center', resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                </View>
                <View style={{ backgroundColor: '#f9f9f9', width: width/1.1, height: height/10, alignSelf: 'center', marginTop: height/30, borderRadius: height/20, flexDirection: 'row'}}>
                    <TextInput style={{ width: width/1.9, marginLeft: width/15, flex:1, fontSize: height/45 }} placeholder={"Search for the perfect role"}/>
                    <Image source={require('../assets/search.png')} style={{height: height/20, width: height/20, alignSelf: 'center', resizeMode: 'contain', right: height/30 }}/>
                </View>
                <View>
                    <FlatList
                        style={{ marginTop: height/20}}
                        data={DATA}
                        renderItem={({ item }) => this.renderEmployee(item)}
                        keyExtractor={item => item.id}
                    />
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    signIn: {
        color: "#D72163",
        alignSelf: "center",
        fontSize: 14,
        marginTop: height / 40,
        marginBottom: 60,
    },
    usernameStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
    },
    passwordStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
        borderWidth: 1
    },
    profPicStyle: {
        height: height/12, 
        width: height/12, 
        borderRadius: height/3,
    }

});