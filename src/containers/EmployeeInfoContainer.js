import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions,
  NetInfo,
  BackHandler,
  Platform
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';
var { height, width } = Dimensions.get('window');

export default class EmployeeInfo extends Component{
    constructor(){
        super();
        this.state = {
            account: false,
            nickName: 'Mark Patterson',
            username: 'Mark P',
            isStartTime: true,
            gender: 'Male',
            email: 'mark@gmail.com',
            phone: '+21 0987 6543 21',
            date: moment('2020-06-12T14:42:42').format("DD-MM-YY"), 
            startTime: moment('2020-06-12T14:42:42').format("LT"),
            endTime: moment('2020-06-12T14:42:42').format("LT"),
            mode: 'date',
            show: false,
        }
    }

    setDate = (event, date) => {
        
        let time = moment(date).format("LT") || moment(this.state.date).format("LT");
        date = moment(date).format("DD-MM-YY") || moment(this.state.date).format("DD-MM-YY");
        this.setState({
            show: Platform.OS === 'ios' ? true : false,
            date,
            startTime: (this.state.isStartTime) ? time : this.state.startTime,
            endTime: (!this.state.isStartTime) ? time: this.state.endTime
        });
    }

    show = mode => {
    this.setState({
        show: true,
        mode,
    });
    }

    datepicker = () => {
    this.show('date');
    }

    timepicker = () => {
    this.show('time');
    }

    renderAccountInfo(){
        return(
            <View>
                <Text style={{ color: "#618ce6", fontSize: height/30, fontWeight: "bold", padding: height/25}}>Account Information</Text>
                <View>
                    <Text style={{ fontSize: height/35, fontWeight: "bold", paddingLeft: height/25, marginBottom: height/80 }}>Nick Name: {this.state.nickName}</Text>
                    <Text style={{ fontSize: height/35, fontWeight: "bold", paddingLeft: height/25, marginBottom: height/80 }}>Username: {this.state.username}</Text>
                    <Text style={{ fontSize: height/35, fontWeight: "bold", paddingLeft: height/25, marginBottom: height/80 }}>Gender: {this.state.gender}</Text>
                    <Text style={{ fontSize: height/35, fontWeight: "bold", paddingLeft: height/25, marginBottom: height/80 }}>Email: {this.state.email}</Text>
                    <Text style={{ fontSize: height/35, fontWeight: "bold", paddingLeft: height/25, marginBottom: height/10 }}>Phone Number: {this.state.phone}</Text>
                </View>

                <TouchableOpacity style={{ alignItems: 'center', width: width/2, alignSelf: 'center', height: height/10, borderRadius: width/100}} onPress={() => this.setState({ account: false })}>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#48cedc', '#49a9e0', '#568ee4']} style={{ justifyContent:'center', height: height/11, width: width/1.3, borderRadius: height/100 }}>
                    <Text style={{ color: 'white', textAlign: 'center', fontWeight: '700'}}>Select Date and Time</Text>
                </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    }

    renderSelectDate(){
        const { show, date, mode } = this.state;
        return(
            <View>
               <Text style={{ color: "#618ce6", fontSize: height/30, fontWeight: "bold", padding: height/35}}>Date and Time</Text>
               <View style={{ marginLeft: height/30, marginBottom: height/45}}>
                <Text style={{ color: "#000", fontSize: height/40, marginBottom: height/66 }}>Select Date</Text>
                <TouchableOpacity onPress={() => this.datepicker()}>
                    <TextInput style={{ borderWidth:2, borderRadius: height/65, borderColor: '#a4a4a4', width: width/1.2, paddingLeft: width/15}} placeholder={this.state.date} pointerEvents="none" editable={false}/>
                </TouchableOpacity>
               </View>
               <View style={{ marginLeft: height/30, marginBottom: height/45}}>
                <Text style={{ color: "#000", fontSize: height/40, marginBottom: height/66 }}>Select Start Time</Text>
                <TouchableOpacity onPress={() => {
                    this.timepicker();
                    this.setState({ isStartTime: true })
                }}>
                    <TextInput style={{ borderWidth:2, borderRadius: height/65, borderColor: '#a4a4a4', width: width/1.2, paddingLeft: width/15}} placeholder={this.state.startTime} pointerEvents="none" editable={false}/>
                </TouchableOpacity>
               </View>
               <View style={{ marginLeft: height/30, marginBottom: height/45}}>
                <Text style={{ color: "#000", fontSize: height/40, marginBottom: height/66 }}>Select End Time</Text>
                <TouchableOpacity onPress={() => {
                    this.timepicker();
                    this.setState({ isStartTime: false })
                }}>
                    <TextInput style={{ borderWidth:2, borderRadius: height/65, borderColor: '#a4a4a4', width: width/1.2, paddingLeft: width/15}} placeholder={this.state.endTime} pointerEvents="none" editable={false}/>
                </TouchableOpacity>
               </View>
                { show && <DateTimePicker 
                        value={new Date()}
                        mode={mode}
                        is24Hour={true}
                        display="spinner"
                        onChange={this.setDate} />
                }

                <TouchableOpacity style={{ alignItems: 'center', width: width/2, alignSelf: 'center', height: height/10, borderRadius: width/100}} onPress={() => this.props.navigation.navigate('EmployeeAppUsage')}>
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#48cedc', '#49a9e0', '#568ee4']} style={{ justifyContent:'center', height: height/11, width: width/1.3, borderRadius: height/100 }}>
                        <Text style={{ color: 'white', textAlign: 'center', fontWeight: '700'}}>Submit</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    }


    render(){
        return(
            <View style={{ backgroundColor: '#FFFFFF', flex:1}}>
                <StatusBar backgroundColor={'black'}/>
                <View style={{ flexDirection: 'row'}}>
                    <TouchableOpacity style={{ width: width/15, marginLeft: height/40}} onPress={() => (this.state.account) ? this.props.navigation.goBack() : this.setState({ account: true })}>
                    <Image source={require('../assets/back.png')} style={styles.backStyle}/>
                    </TouchableOpacity>
                    <Image source={require('../assets/profile_pic.jpeg')} style={styles.profPicStyle}/>
                </View>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#43bfe6', '#0875a3', '#07435c']} style={{ justifyContent:'center', height: height/400, width: width, marginTop: height/50}}/>

                {
                    (this.state.account)
                    ? this.renderAccountInfo()
                    : this.renderSelectDate()
                }
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    signIn: {
        color: "#D72163",
        alignSelf: "center",
        fontSize: 14,
        marginTop: height / 40,
        marginBottom: 60,
    },
    usernameStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
    },
    passwordStyle: {
        backgroundColor: 'white',
        fontSize: height / 65,
        borderBottomWidth: 1,
        borderBottomColor: '#d3d3d3',
        borderWidth: 1
    },
    backStyle:{
        height: height/20, 
        width: height/20, 
        
        marginTop: height/20,
        alignSelf: 'center', 
        resizeMode: 'contain'
    },
    profPicStyle: {
        height: height/7, 
        width: height/7, 
        borderRadius: height/3,
        left: width/4,
        marginTop: height/100
    },
});