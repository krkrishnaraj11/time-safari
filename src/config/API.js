const BASE_URL = 'https://time-safari-env-staging.herokuapp.com/'
export const API = {
    login: BASE_URL + 'users/authenticate',
    register: BASE_URL + 'users/register'
}
