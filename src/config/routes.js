import React, { Component } from 'react';
import { View, Dimensions, Image,Linking } from 'react-native';
import Welcome from '../containers/WelcomeContainer';
import Login from '../containers/LoginContainer';
import AuthLoadingScreen from '../containers/AuthLoadingContainer';
import { createBottomTabNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import SignupEmail from '../containers/SignupEmailContainer';
import SignupPassword from '../containers/SignupPasswordContainer';
import Home from '../containers/HomeContainer';
import Employees from '../containers/EmployeesContainer';
import EmployeeAppUsage from '../containers/EmployeeAppUsageContainer';
import CustomDrawerContentComponent from '../containers/DrawerComponent';
import EmployeeInfo from '../containers/EmployeeInfoContainer';
var { height, width } = Dimensions.get('window');

export const HomeContainer = createAppContainer(createDrawerNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: 'Home'
    }
  },
  Employees: {
    screen: Employees,
    navigationOptions: {
      title: 'Employees'
    }
  },
  EmployeeInfo: {
    screen: EmployeeInfo,
    navigationOptions: {
      title: 'EmployeeInfo`'
    }
  },
  EmployeeAppUsage: {
    screen: EmployeeAppUsage,
    navigationOptions: {
      title: 'EmployeeAppUsage'
    }
  }
}, {
  drawerWidth: width/1.4,
  contentComponent: CustomDrawerContentComponent
}))


export default AppELE = createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Welcome: Welcome,
    LogIn: Login,
    SignupEmail: SignupEmail,
    SignupPassword: SignupPassword,
    Home: HomeContainer
  },
  {
    initialRouteName: 'AuthLoading',
  }
));