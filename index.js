/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Welcome from './src/containers/WelcomeContainer';
import AppELE, { HomeContainer } from './src/config/routes'
import Login from './src/containers/LoginContainer';
import AuthLoadingScreen from './src/containers/AuthLoadingContainer';
import SignupEmail from './src/containers/SignupEmailContainer';
import SignupPassword from './src/containers/SignupPasswordContainer';
import Employees from './src/containers/EmployeesContainer';
import EmployeeInfo from './src/containers/EmployeeInfoContainer';
import EmployeeAppUsage from './src/containers/EmployeeAppUsageContainer';

AppRegistry.registerComponent(appName, () => AppELE);
